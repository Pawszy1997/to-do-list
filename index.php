<?php

//index.php
include('databese.php');
$query = "
 SELECT * FROM test1
 WHERE user_id = '" . $_SESSION["user_id"] . "' 
 ORDER BY zad_id DESC";
$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
    <title>To Do List</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style/style.css">
</head>
<body>
<div class="container">
    <div class="panel-body">
        <form method="post" id="to_do_form">
            <span id="message"></span>
            <div class="input-group">
                <input type="text" name="task_name" id="task_name" class="form-control input-lg" autocomplete="off"
                       placeholder="Enter new task here..."/>
                <div class="input-group-btn">
                    <button type="submit" name="submit" id="submit" class="btn btn-success btn-lg"><span
                                class="glyphicon glyphicon-ok"></span></button>
                </div>
            </div>
        </form>
        <br/>
        <div class="list-group">
            <?php
            foreach ($result as $row) {
                $style = '';
                if ($row["zad_status"] == 'yes') {
                    $style = 'text-decoration: line-through';
                }
                echo '<a href="#" style="' . $style . '" class="list-group-item" id="list-group-item-' . $row["zad_id"] . '" data-id="' . $row["zad_id"] . '">' . $row["zad_text"] . ' <span class="jpg" data-id="' . $row["zad_id"] . '"><img src="style/delete.svg"  width="15px" align="right"></span></a>';
            }
            ?>
        </div>
    </div>
</div>
</div>
</body>
</html>
<script>
    $(document).ready(function () {
        $(document).on('submit', '#to_do_form', function (event) {
            event.preventDefault();

            if ($('#task_name').val() == '') {
                $('#message').html('<div class="alert alert-danger">Enter task</div>');
                return false;
            } else {
                $('#submit').attr('disabled', 'disabled');
                $.ajax({
                    url: "add.php",
                    method: "POST",
                    data: $(this).serialize(),
                    success: function (data) {
                        $('#submit').attr('disabled', false);
                        $('#to_do_form')[0].reset();
                        $('.list-group').prepend(data);
                    }
                })
            }
        });
        $(document).on('click', '.list-group-item', function () {
            var zad_id = $(this).data('id');
            $.ajax({
                url: "update.php",
                method: "POST",
                data: {zad_id: zad_id},
                success: function (data) {
                    $('#list-group-item-' + zad_id).css('text-decoration', 'line-through');
                }
            })
        });

        $(document).on('click', '.jpg', function () {
            var zad_id = $(this).data('id');
            $.ajax({
                url: "delete.php",
                method: "POST",
                data: {zad_id: zad_id},
                success: function (data) {
                    $('#list-group-item-' + zad_id).fadeOut('slow');
                }
            })
        });

    });
</script>
